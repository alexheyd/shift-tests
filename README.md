# Installation

```bash
$ git clone https://alexheyd@bitbucket.org/alexheyd/shift-tests.git
$ cd shift-tests
$ bower install && npm install
$ grunt serve
```

Navigate to [http://localhost:9000](http://localhost:9000)