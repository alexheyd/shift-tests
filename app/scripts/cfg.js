require.config({
    baseUrl:         '/scripts',

    paths: {
        handlebars: '/bower_components/handlebars/handlebars.min',
        jquery:     '/bower_components/jquery/dist/jquery.min',
        backbone:   '/bower_components/backbone/backbone',
        // using lo-dash instead
        underscore: '/bower_components/lodash/dist/lodash.min',
        ModalView:  'views/ModalView'
    },

    shim: {
        'handlebars': {
            exports: 'Handlebars'
        },

        'underscore': {
            exports: '_'
        },
        
        'backbone': {
            deps:    ['underscore', 'jquery'],
            exports: 'Backbone'
        },

        'ModalView': {
            deps:    ['backbone'],
            exports: 'Backbone'
        }
    }
});