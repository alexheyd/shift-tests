var myApp = angular.module('myApp', []);

myApp.controller('CharacterListCtrl', function($scope, $http) {
    // default sort order
    $scope.sortBy = 'house';

    // fetch data, then assign to scope
    $http
        .get('scripts/data/characters.json')
        .then(function(response){
            $scope.characters = response.data;
        });
});