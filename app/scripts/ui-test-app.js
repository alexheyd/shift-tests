define([
        'jquery',
        'underscore',
        'backbone',
        'handlebars',
        'modules/Pubsub',
        'routers/ui-test',
        'ModalView',
        'text!templates/SignInModal.html',
        'text!templates/SignInGoogleModal.html',
        'text!templates/SignInFacebookModal.html',
        'text!templates/SignUpModal.html'
    ], function(
        $,
        _,
        Backbone,
        Handlebars,
        Pubsub,
        UITestRouter,
        ModalView,
        SignInModalTpl,
        SignInGoogleModalTpl,
        SignInFacebookModalTpl,
        SignUpModalTpl
    ) {

    var App = {

        modal: null,

        Router: new UITestRouter(),

        Templates: {
            SignIn:         Handlebars.compile(SignInModalTpl),
            SignInGoogle:   Handlebars.compile(SignInGoogleModalTpl),
            SignInFacebook: Handlebars.compile(SignInFacebookModalTpl),
            SignUp:         Handlebars.compile(SignUpModalTpl)
        },

        ModalView: function(tpl) {
            var template = App.Templates[tpl];
            
            if (!template) return false;

            var view = new ModalView();

            view.setContentTemplate(template);

            return view;

        },

        init: function() {
            _.bindAll(this,
                'showHome',
                'showSignIn',
                'showSignInGoogle',
                'showSignInFacebook',
                'showSignUp',
                'navigate'
            );

            Pubsub.on('app.view.home', this.showHome);
            Pubsub.on('app.view.signIn', this.showSignIn);
            Pubsub.on('app.view.signInGoogle', this.showSignInGoogle);
            Pubsub.on('app.view.signInFacebook', this.showSignInFacebook);
            Pubsub.on('app.view.signUp', this.showSignUp);
            Pubsub.on('app.navigate', this.navigate);

            Backbone.history.start();
        },

        navigate: function(route, opts) {
            App.Router.navigate(route, opts);
        },

        showModal: function(tpl) {
            this.modal = new App.ModalView(tpl);
            this.modal.render();
        },

        hideModal: function() {
            if (this.modal) {
                this.modal.destroy();
            }
        },

        showHome: function() {
            this.hideModal();
        },

        showSignIn: function() {
            this.hideModal();
            this.showModal('SignIn');
        },

        showSignInGoogle: function() {
            this.hideModal();
            this.showModal('SignInGoogle');
        },

        showSignInFacebook: function() {
            this.hideModal();
            this.showModal('SignInFacebook');
        },

        showSignUp: function() {
            this.hideModal();
            this.showModal('SignUp');
        }

    };

    return App;

});
