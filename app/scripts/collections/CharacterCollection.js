define([
        'backbone',
        'models/CharacterModel'
    ], function(
        Backbone,
        CharacterModel
    ) {

    var CharacterCollection = Backbone.Collection.extend({

        model: CharacterModel,

        sortKey: '',

        initialize: function() {
            this.sortBy(this.sortKey);
        },

        comparator: function(a, b) {
            a = a.get(this.sortKey);
            b = b.get(this.sortKey);

            return a > b ?  1
                 : a < b ? -1
                 :          0;
        }, 

        sortBy: function(attr) {
            this.sortKey = attr;
            this.sort();
            return this;
        }
        
    });

    return CharacterCollection;

});