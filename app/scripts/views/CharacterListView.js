define([
        'underscore',
        'backbone',
        'handlebars',
        'text!templates/CharacterList.html',
        'text!templates/CharacterListItem.html'
    ], function(
        _,
        Backbone,
        Handlebars,
        CharacterListTpl,
        CharacterListItemTpl
    ) {

    var CharacterList = Backbone.View.extend({
        
        tagName: 'div',

        template: Handlebars.compile(CharacterListTpl),

        events: {
            'change #sort': 'changeSort'
        },

        // default sort order
        defaultSortBy: 'house',

        initialize: function() {
            // initial sort order
            this.sortCharacters(this.defaultSortBy);

            // register Handlebar partial template for individual Character List Item
            Handlebars.registerPartial('CharacterListItem', CharacterListItemTpl);

            // re-render when collection is sorted
            this.collection.on('sort', _.bind(this.render, this));

        },

        render: function() {
            var data = {
                characters: this.collection.toJSON()
            };

            // to populate sort by dropdown, removing quote as an option
            data.sortBy = _.keys(_.omit(data.characters[0], 'quote'));

            this.$el
                .html(this.template(data))
                // update sort by dropdown value
                .find('#sort').val(this.collection.sortKey);

            return this;
        },

        changeSort: function(event) {
            this.sortCharacters($(event.target).val());
        },

        sortCharacters: function(sortBy) {
            this.collection.sortBy(sortBy);
        }

    });

    return CharacterList;

});