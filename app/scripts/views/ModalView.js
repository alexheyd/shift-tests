define([
        'jquery',
        'underscore',
        'backbone',
        'handlebars',
        'modules/Pubsub',
        'text!templates/Modal.html',
        'text!templates/SignInForm.html',
        'text!templates/SignInMethods.html'
    ], function(
        $,
        _,
        Backbone,
        Handlebars,
        Pubsub,
        ModalTpl,
        SignInTpl,
        SignInMethodsTpl
    ) {

    var ModalView = Backbone.View.extend({
                
        tag: 'div',

        template: Handlebars.compile(ModalTpl),

        events: {
            'click .close-btn':  'closeModal',
            'click #ui-blocker': 'closeModal'
        },

        initialize: function() {
            _.bindAll(this,
                'onKeyUp',
                'center'
            );

            Handlebars.registerPartial('SignInForm', SignInTpl);
            Handlebars.registerPartial('SignInMethods', SignInMethodsTpl);

            $(window)
                .on('keyup.modal', this.onKeyUp)
                .on('resize.modal', this.center);
        },

        onKeyUp: function(event) {
            // ESC key
            if (event.which === 27) this.closeModal();
        },

        closeModal: function() {
            Pubsub.trigger('app.navigate', '', { trigger: false });
            this.destroy();
        },

        setContentTemplate: function(template) {
            Handlebars.registerPartial('ContentTemplate', template);
        },

        center: function() {
            var $win = $(window);

            var win = {
                width: $win.width(),
                height: $win.height()
            };

            var elem = {
                width: this.$el.outerWidth(),
                height: this.$el.outerHeight()
            };

            var center = {
                top: (win.height/2) - (elem.height/2) + $win.scrollTop(),
                left: (win.width/2) - (elem.width/2)
            };

            this.$el.css(center);
        },

        blockUI: function() {
            var $blocker = $('#ui-blocker');
            
            if (!$blocker.length) $blocker = $('<div id="ui-blocker" />');

            $blocker.on('click', _.bind(this.closeModal, this));

            $('body').append($blocker);
        },

        unblockUI: function() {
            $('#ui-blocker').off('click').remove();
        },
        
        render: function() {
            this.blockUI();

            this.$el
                .addClass('modal')
                .html(this.template())
                .appendTo($('body'));

            this.center();

            this.$el.addClass('show');

            return this;
        },

        destroy: function() {
            $(window).off('.modal');
            this.unblockUI();
            this.$el.remove();
        }

    });

    return ModalView;

});