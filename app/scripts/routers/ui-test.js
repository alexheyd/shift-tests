define([
        'backbone',
        'modules/Pubsub'
    ], function(
        Backbone,
        Pubsub
    ) {
   
    var Router = Backbone.Router.extend({
        routes: {
            '':                'home',
            'signin':          'signIn',
            'signin-google':   'signInGoogle',
            'signin-facebook': 'signInFacebook',
            'signup':          'signUp'
        },

        home: function() {
            Pubsub.trigger('app.view.home');
        },

        signIn: function() {
            Pubsub.trigger('app.view.signIn');
        },

        signInGoogle: function() {
            Pubsub.trigger('app.view.signInGoogle');
        },

        signInFacebook: function() {
            Pubsub.trigger('app.view.signInFacebook');
        },

        signUp: function() {
            Pubsub.trigger('app.view.signUp');
        }
    });
   
    return Router; 
});