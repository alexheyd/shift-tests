define([
        'underscore',
        'backbone',
        'collections/CharacterCollection',
        'views/CharacterListView'
    ], function(
        _,
        Backbone,
        CharacterCollection,
        CharacterListView
    ) {

    var CharacterList = function CharacterList(options) {

        this.data       = null;
        this.characters = null;

        this.init = function() {

            if (!options.el || !options.characters) {
                return false;
            }

            this.$el = options.el;

            // sets Characters data, parses a JSON string if needed
            this.setData(options.characters);

            // if invalid data was passed in, stop
            if (!this.data) return false;

            this.characters = new CharacterCollection(this.data);
            
            this.view = new CharacterListView({
                collection: this.characters
            });

        };

        this.render = function() {
            this.$el.html(this.view.render().$el);
        };

        this.setData = function(chars) {
            if (_.isObject(chars)) {
                this.data = chars;
            }
            else if (_.isString(chars)) {
                this.data = JSON.parse(chars);
            }
        };

        this.init();

    };

    return CharacterList;

});