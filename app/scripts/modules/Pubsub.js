define([
        'underscore',
        'backbone'
    ], function(
        _,
        Backbone
    ) {
   
    var Pubsub = _.extend({}, Backbone.Events);
   
    return Pubsub; 
});