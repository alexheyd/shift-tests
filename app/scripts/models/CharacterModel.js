define([
        'backbone'
    ], function(
        Backbone
    ) {

    var CharacterModel = Backbone.Model.extend({
        defaults: {
            "name":     null,
            "house":    null,
            "actor":    null,
            "quote":    null,
            "episodes": null
        }
    });

    return CharacterModel;

});