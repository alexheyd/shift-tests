require([
        'jquery',
        'text!data/characters.json',
        'modules/CharacterList'
    ], function(
        $,
        CharacterData,
        CharacterList
    ) {

    var list = new CharacterList({
        el: $('#character-list'),
        characters: CharacterData
    });

    list.render();

});
